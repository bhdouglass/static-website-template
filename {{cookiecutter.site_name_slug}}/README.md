# {{cookiecutter.site_name}}

{{cookiecutter.description}}

## Development

* Make a copy of this template
* Install the needed tools
    * [Gulp](http://gulpjs.com)
    * [Bower](http://bower.io)
* Install dependencies
    * Run `bower install`
    * Run `npm install`
* Start the dev web server
    * Run `gulp server`
* Navigate to (http://localhost:8080)[http://localhost:8080] in your browser
* Deploy your website
    * Run `gulp deploy`
* Profit!
