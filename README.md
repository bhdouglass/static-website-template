# Brian's Opinionated Static Website Template

This is an opinionated website template to help quickly
bootstrap a new website. It includes:

* [Cookiecutter](https://cookiecutter.readthedocs.io/en/latest/) generating the project
* A [Gulp](http://gulpjs.com) lint/build/deploy system
* Default [Bower](http://bower.io) components
    * [Bootstrap](http://getbootstrap.com)
    * [Bootstrap Material Design](http://fezvrasta.github.io/bootstrap-material-design/)
    * [Font Awesome](http://fontawesome.io)
* [Less](http://lesscss.org)
* [Nunjucks](https://mozilla.github.io/nunjucks/) HTML templates
* Deployment to [Surge](https://surge.sh)

## Usage

* Install the needed tools
    * [Cookiecutter](https://cookiecutter.readthedocs.io/en/latest/)
    * [Gulp](http://gulpjs.com)
    * [Bower](http://bower.io)
* Generate the project
    * Run `cookiecutter gh:bhdouglass/static-website-template`
* Go into the directory for your new project
* Install dependencies in your new project
    * Run `bower install`
    * Run `npm install`
* Start the dev web server
    * Run `gulp server`
* Navigate to [http://localhost:8080](http://localhost:8080) in your browser
* Deploy your website
    * Run `gulp deploy`
* ???
* Profit!

## License

Copyright (C) 2017 [Brian Douglass](http://bhdouglass.com/)

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3, as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
